# inverse kinematics

1. install [moveit](https://moveit.ros.org/install/)

2. clone
```
git clone https://gitlab.com/Yonder-Dynamics/controls/moveit.git ~/catkin_ws/
```

3. compile
```
cd ~/catkin_ws/ && catkin_make
```

4. source
```
source ~/catkin_ws/devel/setup.zsh
```

currently configured to work with [apathy_hardware_inteface](https://gitlab.com/Yonder-Dynamics/controls/ros_control_apathy)

`roslaunch arm_moveit_config demo.launch`

then move it and plan and execute

Am going to add in gazebo integration with [rover_simulation](https://gitlab.com/Yonder-Dynamics/misc/rover_simulation)

Alternatively, do

`roslaunch arm_moveit_config arm_moveit_control.launch 2>/dev/null`

Note we are ignoring the erros currently. When the launch file finishes, you can press enter to cycle through differnt heights in the cartesian coordinate. Use ctrl+D to exit.

Requires tkinter. Install with
`sudo apt install python-tk`