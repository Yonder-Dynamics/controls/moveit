#!/usr/bin/env python

import sys
import copy
import rospy
import time
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
from std_msgs.msg import String
from Tkinter import *

class Arm(object):
  def __init__(self):
    super(Arm, self).__init__()

    moveit_commander.roscpp_initialize(sys.argv)
    rospy.init_node('move_group_python_interface',
                    anonymous=True)

    self.robot = moveit_commander.RobotCommander()

    self.group_name = "arm"
    self.group = moveit_commander.MoveGroupCommander(self.group_name)

    self.display_trajectory_publisher = rospy.Publisher('/move_group/display_planned_path',
                                                   moveit_msgs.msg.DisplayTrajectory,
                                                   queue_size=20)

    self.planning_frame = self.group.get_planning_frame()

    # We can also print the name of the end-effector link for this group:
    self.eef_link = self.group.get_end_effector_link()

    # We can get a list of all the groups in the robot:
    self.group_names = self.robot.get_group_names()

    # robot:
    print("============ Printing robot state")
    print(self.robot.get_current_state())

  def move(self, d_pos):
    """ move by amount """

  def move_to(self, pos):
    """ move to position """

    # set desired pose
    self.group.set_pose_target(pos)

    # execute without waiting
    self.group.go(wait=False)

    #self.group.stop()
    #self.group.clear_pose_targets()

def gotoPt():
    pose_goal.position.x = x.get()/100.0
    pose_goal.position.y = y.get()/-100.0
    pose_goal.position.z = z.get()/100.0
    print(pose_goal.position)
    robot.move_to(pose_goal)
    
# simple unit tests. press enter to cycle through z
if __name__ == "__main__":
    robot = Arm()
    pose_goal = geometry_msgs.msg.Pose()
    pose_goal.orientation.w = 1.0
    pose_goal.position.x = 0.5
    pose_goal.position.y = 0.5
    pose_goal.position.z = 0 # initial pose

    master = Tk()
    x = Scale(master, from_=32, to=80, label="x") # arm front back
    x.pack()
    y = Scale(master, from_=-50, to=50, label="y", orient=HORIZONTAL) # left right
    y.pack()
    z = Scale(master, from_=0, to=60, label="z") # up down
    z.pack()
    Button(master, text='gotoPt', command=gotoPt).pack() # extra button

    last_coord = [0, 0, 0]
    while True:
      time.sleep(0.1)
      master.update()
      cur_coord = [x.get(), y.get(), z.get()]
      if cur_coord == last_coord: continue # stop commands once no new input comes
      last_coord = cur_coord
      gotoPt() # executes
      